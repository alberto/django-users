from django.db import models

# Create your models here.
class Contenido(models.Model):
    clave=models.CharField(max_length=64)#recurso
    valor=models.TextField()#HTML

class Comentario(models.Model):
    contenido=models.ForeignKey(Contenido,on_delete=models.CASCADE)#para linkear con la clave primaria de contenidp
    titulo=models.CharField(max_length=128)
    comentario=models.TextField()
    fecha=models.DateTimeField()