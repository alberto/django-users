from django.shortcuts import render
from django.http import HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from .models import Contenido
from django.template import loader
from django.contrib.auth import logout
from django.shortcuts import redirect
# Create your views here.
def index(request):
    content_list=Contenido.objects.all()
    template=loader.get_template('cms_users/index.html')
    contexto={'content_list':content_list}
    return HttpResponse(template.render(contexto,request))

@csrf_exempt
def get_resource(request, recurso):

    if request.method in ["PUT","POST"]:
        if request.method=="POST":
            Valor=request.POST["valor"]
        else:
            Valor = request.body.decode()#da problemas con los +

        try:
            contenido = Contenido.objects.get(clave=recurso)
            contenido.valor=Valor
            contenido.save()
        except Contenido.DoesNotExist:
            contenido = Contenido(clave=recurso, valor=Valor)
            contenido.save()
    try:
        contenido = Contenido.objects.get(clave=recurso)
    except Contenido.DoesNotExist:
            if request.user.is_authenticated:
                template = loader.get_template('cms_users/formulario.html')
                contexto = {}
                return HttpResponse(template.render(contexto, request))
            else:
                return HttpResponse("Para poder dar de alta un recurso tienes que estar <a href=/login>autentificado</a>")

    template = loader.get_template('cms_users/respuesta.html')
    contexto = {'contenido': contenido}
    return HttpResponse(template.render(contexto, request))

def loggedIn(request):
    if request.user.is_authenticated:
        respuesta="Bienvenido "+request.user.username+" al CMS de SARO"
    else:
        respuesta="No estas autentificado ,<a href=/admin>autentícate</a>"

    return HttpResponse(respuesta)

def logout_vista(request):
    logout(request)
    return redirect("/cms_users")